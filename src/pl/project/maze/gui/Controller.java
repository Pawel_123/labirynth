package pl.project.maze.gui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import pl.project.maze.Field;
import pl.project.maze.Maze;
import pl.project.maze.MazeSolver;
import pl.project.maze.Point;

import java.io.IOException;
import java.util.List;

public class Controller {

    @FXML
    private GridPane gridPane;

    @FXML
    private Button resolve;


    private Label[][] labels;
    private Point currentPoint;
    private Maze maze;


    public void initialize() throws IOException {


        maze = new Maze("maze.txt");
        currentPoint = maze.getStartPoint();
        labels = new Label[maze.getxSize()][maze.getySize()];
        for (int x = 0; x < maze.getySize(); x++) {
            for (int y = 0; y < maze.getxSize(); y++) {
                Label label = new Label();
                label.setPrefHeight(20);
                label.setPrefWidth(20);
                labels[x][y] = label;
                styleLabel(new Point(x, y));
                gridPane.add(label, x, y);
            }
        }

    }

    private void styleLabel(Point point) {
        Label label = labels[point.getX()][point.getY()];
        if (point.equals(currentPoint)) {
            label.setStyle("-fx-background-color: chocolate");
        } else {
            switch (maze.getFieldAt(point)) {
                case WALL:
                    label.setStyle("-fx-background-color: black");
                    break;
                case START:
                    label.setStyle("-fx-background-color: green");
                    break;
                case END:
                    label.setStyle("-fx-background-color: red");
                    break;
                case PATH:
                    label.setStyle("-fx-background-color: white");
                    break;
            }
        }
    }

    public void onRightButtonClick() {
        int x = currentPoint.getX() + 1;
        int y = currentPoint.getY();
        movePoint(x, y);
    }

    public void onLeftButtonClick() {
        int x = currentPoint.getX() - 1;
        int y = currentPoint.getY();
        movePoint(x, y);

    }

    public void onUpButtonClick() {
        int x = currentPoint.getX();
        int y = currentPoint.getY() - 1;
        movePoint(x, y);

    }

    public void onDownButtonClick() {
        int x = currentPoint.getX();
        int y = currentPoint.getY() + 1;
        movePoint(x, y);

    }

    public void onResolveButtonClick() {
        MazeSolver mazeSolver = new MazeSolver(maze);
        List<Point> points = mazeSolver.solve(currentPoint);
        Thread solutionThread = new Thread(() -> {
            points.forEach(point -> {
                movePoint(point.getX(), point.getY());

                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        });
        solutionThread.setDaemon(true);
        solutionThread.start();
    }


    private void movePoint(int x, int y) {
        Point newCurrentPoint = new Point(x, y);
        Field field = maze.getFieldAt(newCurrentPoint);
        if (field != null && field != Field.WALL) {
            Point oldCurrentPoint = currentPoint;
            currentPoint = newCurrentPoint;
            styleLabel(oldCurrentPoint);
            styleLabel(currentPoint);
        }
    }
}



