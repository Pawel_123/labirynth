package pl.project.maze;

public class State {
    private Point point;
    private int scoreFnValue;
    private State origin;
    private boolean visited;

    public State(Point point, int scoreFnValue, State origin) {
        this.point = point;
        this.scoreFnValue = scoreFnValue;
        this.origin = origin;
        this.visited = false;
    }

    public Point getPoint() {
        return point;
    }

    public int getScoreFnValue() {
        return scoreFnValue;
    }

    public State getOrigin() {
        return origin;
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() {
        visited = true;
    }
}
