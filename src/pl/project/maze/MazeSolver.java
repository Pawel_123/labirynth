package pl.project.maze;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MazeSolver {

    private Maze maze;
    List<State> stateList;

    public MazeSolver(Maze maze) {
        this.maze = maze;
    }

    public List<Point> solve() {
        return solve(maze.getStartPoint());
    }

    public List<Point> solve(Point startPoint) {
        stateList = new ArrayList<>();
        State state = new State(startPoint, calculateScoreFnValue(startPoint), null);
        stateList.add(state);
        while (!state.getPoint().equals(maze.getEndPoint())) {
            visit(state);
            state = findBestUnvisitedState();
        }
        List<Point> points = new ArrayList<>();
        while (!state.getPoint().equals(startPoint)) {
            points.add(state.getPoint());
            state = state.getOrigin();
        }
        points.add(startPoint);
        Collections.reverse(points);
        return points;
    }

    private State findBestUnvisitedState() {

        return stateList.stream()
                .filter(state -> !state.isVisited())
                .min((state1, state2) -> state1.getScoreFnValue() - state2.getScoreFnValue())
                .orElseThrow(() -> new RuntimeException("Skończyły się nieodwiedzone stny"));
    }

    private void visit(State state) {
        state.visit();

        int x = state.getPoint().getX();
        int y = state.getPoint().getY();
        List<Point> surroundingsPoints = Arrays.asList(new Point(x - 1, y),
                new Point(x + 1, y),
                new Point(x, y - 1),
                new Point(x, y + 1));
        surroundingsPoints.stream()
                .filter(point -> {
                    Field field = maze.getFieldAt(point);
                    return field != null && field != Field.WALL && !doesStateExistForPoint(point);
                }).forEach(point -> stateList.add(new State(point, calculateScoreFnValue(point), state)));
    }

    private boolean doesStateExistForPoint(Point point) {
        return stateList.stream().anyMatch(state -> state.getPoint().equals(point));
    }

    private void addIfExist(State state) {
        for (int i = 0; i < stateList.size(); i++) {
            if (!state.equals(stateList.get(i))) {
                stateList.add(state);
            }
        }

    }

    private int calculateScoreFnValue(Point point) {
        Point endPoint = maze.getEndPoint();

        return Math.abs(endPoint.getX() - point.getX()) + Math.abs(endPoint.getY() - point.getY());
    }
}
