package pl.project.maze;

import java.io.IOException;
import java.util.List;

public class Test {
    public static void main(String[] args) throws IOException, InterruptedException {

//        System.out.println(new Maze("maze.txt"));

        Maze maze = new Maze("maze.txt");

        MazeSolver mazeSolver = new MazeSolver(maze);
//        mazeSolver.solve().forEach(s-> System.out.println(s));
        List<Point> points = mazeSolver.solve();
        List<Point> solution = new MazeSolver(maze).solve();
//        System.out.println(points.size());
//        points.forEach(s->maze.printWithPoint(s));

        solution.stream().forEach(point -> {
            maze.printWithPoint(point);
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


    }
}
