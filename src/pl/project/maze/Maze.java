package pl.project.maze;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Maze {
    private Field[][] fields;
    private int xSize;
    private int ySize;
    private Point startPoint;
    private Point endPoint;

    public Maze(String filename) throws IOException {
        Path path = Paths.get(filename);
        List<String> fileLines = Files.readAllLines(path);
        xSize = fileLines.get(0).length();
        ySize = fileLines.size();
        fields = new Field[xSize][ySize];
        fillFields(fileLines);
    }

    public int getxSize() {
        return xSize;
    }

    public int getySize() {
        return ySize;
    }

    public void start() throws InterruptedException {
        int y = 1;
        int x = 0;
        boolean end = false;
        while (!end) {
            if (fields[x][y] == Field.START) {
                System.out.println("Welcome in the game. Computer will be going through maze by right hand rule");
                Thread.sleep(100);
            }
            if (fields[x][y + 1] == Field.PATH) {
                System.out.println("Move down ");
                Thread.sleep(100);
                y++;
            } else if (fields[x][y + 1] == Field.WALL && fields[x + 1][y] == Field.PATH) {
                System.out.println("Move forward");
                Thread.sleep(100);
                x++;
            } else if (fields[x + 1][y] == Field.WALL && fields[x][y + 1] == Field.WALL && fields[x][y - 1] == Field.PATH) {
                System.out.println("Move upward");
                Thread.sleep(100);
                y--;

            } else if (fields[x][y - 1] == Field.WALL && fields[x + 1][y] == Field.WALL && fields[x][y + 1] == Field.WALL && fields[x - 1][y] == Field.PATH) {
                System.out.println("Move backward");
                Thread.sleep(100);
                x--;

            }
            if (fields[x][y] == Field.END) {
                System.out.println("I finished it");
                end = true;
            }
        }

    }

    private void fillFields(List<String> fileLines) {
        for (int y = 0; y < ySize; y++) {
            String fileLine = fileLines.get(y);
            for (int x = 0; x < xSize; x++) {
                switch (fileLine.charAt(x)) {
                    case '+':
                    case '-':
                    case '|':
                        fields[x][y] = Field.WALL;
                        break;
                    case ' ':
                        fields[x][y] = Field.PATH;
                        break;
                    case 'S':
                        fields[x][y] = Field.START;
                        startPoint = new Point(x, y);
                        break;
                    case 'E':
                        fields[x][y] = Field.END;
                        endPoint = new Point(x, y);
                        break;
                }

            }

        }
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public Field getFieldAt(Point point) {
        if (point.getX() < 0 || point.getX() >= xSize || point.getY() < 0 || point.getY() >= ySize) {
            return null;
        } else {
            return fields[point.getX()][point.getY()];
        }
    }

    public void printWithPoint(Point point) {
        StringBuilder builder = new StringBuilder();
        for (int y = 0; y < ySize; y++) {
            for (int x = 0; x < xSize; x++) {
                if (point.getX() == x && point.getY() == y) {
                    builder.append(".");
                } else {
                    switch (fields[x][y]) {
                        case START:
                            builder.append("S");
                            break;
                        case END:
                            builder.append("E");
                            break;
                        case PATH:
                            builder.append(" ");
                            break;
                        case WALL:
                            builder.append("*");
                    }
                }
            }
            builder.append("\n");
        }
        System.out.println(builder.toString());

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int y = 0; y < ySize; y++) {
            for (int x = 0; x < xSize; x++) {
                switch (fields[x][y]) {
                    case START:
                        builder.append("S");
                        break;
                    case END:
                        builder.append("E");
                        break;
                    case PATH:
                        builder.append(" ");
                        break;
                    case WALL:
                        builder.append("*");
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }
}
